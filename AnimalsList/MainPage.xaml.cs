﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AnimalsList
{
	// Learn more about making custom code visible in the Xamarin.Forms previewer
	// by visiting https://aka.ms/xamarinforms-previewer
	[DesignTimeVisible(false)]
	public partial class MainPage : ContentPage , INotifyPropertyChanged
	{
		Animals animals = new Animals();
		public MainPage()
		{
			InitializeComponent();
		//	NavigationPage.SetHasNavigationBar(this, false);	
			ListofAnimals.ItemsSource = animals.data;
		}

		private async void ListofAnimals_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			var mydetails = e.Item as AnimalsListModel;
			if (mydetails == null)
			{
				return;
			}
			else
			{
				await Navigation.PushAsync(new AnimalDescription(mydetails.AnimalDescription, mydetails.AnimalName, mydetails.AnimalsImage));
				ListofAnimals.SelectedItem = null;
			}
		}

		private void MenuItem_Clicked(object sender, EventArgs e)
		{
			var menuItem = sender as MenuItem;
			AnimalsListModel ghji = new AnimalsListModel();
			string xy = menuItem.CommandParameter.ToString();
			ghji = animals.data.Where(x => x.AnimalName == xy).FirstOrDefault();
			animals.data.Remove(ghji);
			ListofAnimals.ItemsSource = null;
			ListofAnimals.ItemsSource = animals.data;
		}
		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(
		[CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this,
			new PropertyChangedEventArgs(propertyName));
		}









	}
}
