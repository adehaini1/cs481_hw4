﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AnimalsList
{
	public class AnimalsListViewModel : INotifyPropertyChanged
	{
		Animals animals = new Animals();
		public AnimalsListViewModel()
		{
			AnimalListItemSource = animals.data;
			
		}

		

		

		public  Command Refresh
		{
			get
			{
				return new Command(async () =>
				{
					IsLoading = true;
					await Task.Delay(3000);
					IsLoading = false;					
				}
			  );
			}
		}

		


		private ObservableCollection<AnimalsListModel> _item;
		public ObservableCollection<AnimalsListModel> AnimalListItemSource { get { return _item; } set { _item = value; OnPropertyChanged(); } }


		private bool _isloading;
		public bool IsLoading { get { return _isloading; } set { _isloading = value;OnPropertyChanged(); } }
		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(
	    [CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this,
			new PropertyChangedEventArgs(propertyName));
		}
	}
}
