﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace AnimalsList
{
	public class AnimalsListModel 
	{
		public string AnimalsImage { get; set; }
		public string AnimalIcon { get; set; }
		public string AnimalName { get; set; }
		public string AnimalDescription { get; set; }

		
	}

	public class Animals : INotifyPropertyChanged
	{
		public Animals() 
		{
			data = new ObservableCollection<AnimalsListModel>()
			 {
				new AnimalsListModel()
						 {
				  AnimalName="Alligator", AnimalsImage="AlligatorImage.jpg", AnimalIcon="Alligator.png", AnimalDescription= String.Join(Environment.NewLine,"Both males and females have an armored body with a muscular, flat tail. The skin on their back is armored with embedded bony plates called osteoderms or scutes. They have four short legs; the front legs have five toes while the back legs have only four toes. Alligators have a long, rounded snout that has upward facing nostrils at the end; this allows breathing to occur while the rest of the body is underwater. The young have bright yellow stripes on the tail adults have dark stripes on the tail","","It's easy to distinguish an alligator from a crocodile by the teeth. The large, fourth tooth in the lower jaw of an alligator fits into a socket in the upper jaw and is not visible when the mouth is closed. This does not happen in crocodiles. Alligators have between 74 and 80 teeth in their mouth at a time. As they wear down, they are replaced. An alligator can go through 3,000 teeth in a lifetime.” -Smithsonian National Zoo ")

					   },
				new AnimalsListModel()
						 {
				  AnimalName="Alpaca", AnimalsImage="AlpacaImage.png", AnimalIcon="Alpaca.png", AnimalDescription= String.Join(Environment.NewLine,"“Alpacas are slender-bodied animals with long legs and necks, small heads and large, pointed ears. They are covered in a soft fleece that is virtually free of guard hair and occurs in a variety of colors. Alpacas have soft, padded feet that leave even the most delicate grasses and terrain undamaged as they graze. The two types of alpacas are huacaya and suri.","","Ninety-five percent of alpacas are huacayas. Their crimpy fiber grows perpendicular to the skin, giving them a wooly appearance. Suris have straight fiber that curls toward the ground, hanging in dreadlocks. However, because of their finer, less dense coat, the suri is not quite as adept at withstanding severe weather. The suri is rarer with only about 5,000 registered in the United States.","","The Smithsonian's National Zoo exhibits huacaya alpaca. Alpacas have no top, front teeth. An adult male alpaca's upper and lower incisors and lower canines develop into fighting teeth or fangs that can be more than 1.2 inches (3 centimeters) long. These teeth do not develop as much in females. Aside from this difference in tooth morphology, sexual dimorphism is minimal in alpacas.” – Smithsonian National Zoo")


					   },
				new AnimalsListModel()
						 {
				  AnimalName="Lion", AnimalsImage="LionImage.png", AnimalIcon="Lion.png", AnimalDescription= String.Join(Environment.NewLine,"“Lions have strong, compact bodies and powerful forelegs, teeth and jaws for pulling down and killing prey. Their coats are yellow-gold, and adult males have shaggy manes that range in color from blond to reddish-brown to black. The length and color of a lion's mane is likely determined by age, genetics and hormones. Young lions have light spotting on their coats that disappears as they grow.","","Without their coats, lion and tiger bodies are so similar that only experts can tell them apart.” -Smithsonian National Zoo","","“Lions in zoos may live into their late teens or early 20s. In the wild, a lioness may live up to 16 years, but males rarely live past the age of 12.” -Smithsonian National Zoo")


					   },
				new AnimalsListModel()
						 {
				  AnimalName="Meerkat", AnimalsImage="MeerkatImage.jpg", AnimalIcon="Meerkat.png", AnimalDescription= String.Join(Environment.NewLine,"“A member of the mongoose family, meerkats (also known as suricates) have grizzled gray and brown colored coats of fur with dark patches around their eyes, which help protect their eyes from the glare of the sun. They also have a dark tip on the tail. Meerkats have powerful foreclaws for digging. Their pointed snout helps enable them to excavate prey from narrow trenches.” -Smithsonian National Zoo")
			},
				new AnimalsListModel()
						 {
				  AnimalName="Orangutan", AnimalsImage="OrangutanImage.png", AnimalIcon="Orangutan.png", AnimalDescription= String.Join(Environment.NewLine,"“Orangutans have long, sparse orange or reddish hair unequally distributed over their bodies. They have large jaws and flattened noses in concave faces.","","Orangutans are the largest arboreal mammals and are very well adapted to life in the trees, with arms much longer than their legs. They have grasping hands and feet with long curved fingers and toes. They have distinctive fingerprints and no visible external tails.","","You can typically tell male and female orangutans apart by looking at them. Males and females have flabby throat sacs, which become very large in adult males. Adult males have deep chests and much longer body hair than females do. Males also typically develop large cheek pads, which demonstrate genetic fitness and amplify their long calls.","","Orangutans can brachiate—swing hand over hand—but they normally move cautiously through large trees by climbing and walking. This allows them to distribute their weight among the branches. Orangutans' hands make them graceful and swift while swinging, but it makes walking on the ground very slow and awkward.","","Orangutans sometimes travel on the ground when going long distances because appropriate sized branches may not always be available. When on the ground, they use all four limbs, supporting themselves on the sides of clenched fists, or occasionally walk on upright on two legs. Orangutans also come down if there is a need to find food and water elsewhere, for example, if there is a drought or fire.","","It can be difficult to tell Sumatran and Bornean orangutans apart. Generally, Sumatran orangutans are lighter in color, have longer body hair and less pendulous throat sacs than Bornean orangutans, but the only reliable way to tell the difference between Sumatran and Bornean orangutans is by looking at their chromosomes.” – Smithsonian National Zoo ","","“The median life expectancy for male Bornean orangutans is about 27 years and for male Sumatran orangutans is about 25 years. The median life expectancy for female Sumatran orangutans is about 32 years. There is not enough available data on the life expectancy of female Bornean orangutans.” - Smithsonian National Zoo ")
			},
				new AnimalsListModel()
						 {
				  AnimalName="Panda", AnimalsImage="PandaImage.jpg", AnimalIcon="Panda.png", AnimalDescription= String.Join(Environment.NewLine,"“Scientists are not sure how long giant pandas live in the wild, but they are sure it is shorter than lifespans in zoos. They estimate that lifespan is about 15-20 years for wild pandas and about 30 years for those in human care. Chinese scientists have reported zoo pandas as old as 35.” -Smithsonian National Zoo","","“The giant panda, a black-and-white bear, has a body typical of bears. It has black fur on its ears, eye patches, muzzle, legs, and shoulders. The rest of the animal's coat is white. Although scientists do not know why these unusual bears are black and white, some speculate that the bold coloring provides effective camouflage. In patches of dense bamboo, an immobile giant panda is nearly invisible, and virtually disappears among snow covered rocky outcrops on a mountain slope. This theory does not work, however, when considering that giant pandas have no natural enemies to hide from. Another thought is that the pattern may accentuate social signals in some way, or help giant pandas to identify one another from a distance so they can avoid socializing, as they are typically a solitary animal. Another theory suggests that the black absorbs heat while the white reflects it, helping giant pandas maintain an even temperature. Unfortunately, there is no one conclusive theory as to why giant pandas are black and white.","","The giant panda has lived in bamboo forests for several million years. It is a highly specialized animal, with unique adaptations. The panda's thick, wooly coat keeps it warm in the cool forests of its habitat. Giant pandas have large molar teeth and strong jaw muscles for crushing tough bamboo. Many people find these chunky, lumbering animals to be cute, but giant pandas can be as dangerous as any other bear.” -Smithsonian National Zoo")
			},
				new AnimalsListModel()
						 {
				  AnimalName="Tiger", AnimalsImage="TigerImage.png", AnimalIcon="Tiger.png", AnimalDescription= String.Join(Environment.NewLine,"“Tigers have reddish-orange coats with prominent black stripes, white bellies and white spots on their ears. Like a human fingerprint, no two tigers have the exact same markings. Because of this, researchers can use stripe patterns to identify different individuals when studying tigers in the wild. Tigers are powerful hunters with sharp teeth, strong jaws and agile bodies. They are the largest terrestrial mammal whose diet consists entirely of meat. The tiger's closest relative is the lion. In fact, without fur, it is difficult to distinguish a tiger from a lion.” -Smithsonian National Zoo” -Smithsonian National Zoo","","“The life span of tigers in the wild is usually between 10 and 15 years. In human care, or on rare occasions in the wild, a tiger can live up to 20 years. However, approximately half of all wild tiger cubs do not survive past the first two years of life. Only 40 percent of those that reach independence actually live to establish a territory and produce young. The risk of mortality remains high for adult tigers due to their territorial nature, which often results in direct competition with conspecifics, or members of the same species” -Smithsonian National Zoo \n\n\n Work Cited nationalzoo.si.edu A special thanks to the Smithsonian National zoo for providing all the animal facts created for this app.")
			}

			 };
		}
		private ObservableCollection<AnimalsListModel> _isloading;
		public ObservableCollection<AnimalsListModel> data { get { return _isloading; } set { _isloading = value; OnPropertyChanged(); } }
		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(
	[CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this,
			new PropertyChangedEventArgs(propertyName));
		}
	}
}
